#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias ssh='ssh -Y'
PS1='[\u@\h \W]\$ '

if [ -f /etc/bash_completion ]; then
    .   /etc/bash_completion
fi


# PATH messyness
export GADGETRON_HOME=/usr/local/gadgetron
export PATH=$PATH:/usr/local/gadgetron/bin:/usr/local/ismrmrd/bin:/home/anderson/scripts
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/gadgetron/lib:/usr/local/ismrmrd/lib:/usr/local/lib
export EDITOR=/usr/bin/vim
