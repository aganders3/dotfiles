# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt appendhistory beep extendedglob nomatch notify
unsetopt autocd
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/anderson/.zshrc'

autoload -U zmv
autoload -Uz compinit
compinit
# End of lines added by compinstall

# autoload -U promptinit
# promptinit

autoload -U colors && colors
PROMPT="[%{$fg_bold[green]%}%n@%m%{$reset_color%}] %# "
RPROMPT="[%{$fg_bold[yellow]%}%~%{$reset_color%}]"

export LSCOLORS="Gxfxcxdxbxegedabagacad"
alias ls='ls -G'
alias ssh='ssh -y'
alias note='ipython notebook'
alias notes='ipcluster start -n 8 && ipython notebook'
alias kdiff3='open /applications/kdiff3.app/'
alias gpi='gpi --nosplash'
alias du='du -sh'

export PATH=/opt/gpi/bin:/opt/gpi/local/anaconda/bin:$PATH
export DYLD_FALLBACK_LIBRARY_PATH=/opt/gpi/local/anaconda/lib:$DYLD_FALLBACK_LIBRARY_PATH

export editor=/usr/bin/vim

