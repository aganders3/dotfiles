"------------------------------------------------------------------------------
" File: $HOME/.vimrc
" Author: Ashley Anderson III <aganders3@gmail.com> 
"------------------------------------------------------------------------------
"------------------------------------------------------------------------------
" Standard stuff.
"------------------------------------------------------------------------------
set guifont=Ubuntu\ Mono:h20
set t_Co=256
set colorcolumn=80
colorscheme wombat256
" set lines=55 "
" set columns=95 "
set nocompatible        " Disable vi compatibility.
set nobackup            " Do not keep a backup file.
set history=100         " Number of lines of command line history.
set undolevels=200      " Number of undo levels.
set textwidth=0         " Don't wrap words by default.
set number              " Show line numbers
set showcmd             " Show (partial) command in status line.
set showmatch           " Show matching brackets.
set showmode            " Show current mode.
set ruler               " Show the line and column numbers of the cursor.
set ignorecase          " Case insensitive matching.
set incsearch           " Incremental search.
" set noautoindent        " I indent my code myself.
" set nocindent           " I indent my code myself.
set scrolloff=5         " Keep a context when scrolling.
set nodg                " Required for e.g. German umlauts.
set noerrorbells        " No beeps.
" set nomodeline          " Disable modeline.
set modeline            " Enable modeline.
set esckeys             " Cursor keys in insert mode.
" set gdefault            " Use 'g' flag by default with :s/foo/bar/.
set magic               " Use 'magic' patterns (extended regular expressions).
set expandtab           " use spaces instead of tabs
set tabstop=4           " Number of spaces <tab> counts for.
set shiftwidth=4
set ttyscroll=0         " Turn off scrolling (this is faster).
set ttyfast             " We have a fast terminal connection.
set hlsearch            " Highlight search matches.
" set encoding=utf-8      " Set default encoding to UTF-8.
set showbreak=+         " Show a '+' if a line is longer than the screen.
set nostartofline       " Do not jump to first character with page commands,
                        " i.e., keep the cursor in the current column.
set viminfo='20,\"50    " Read/write a .viminfo file, don't store more than
                        " 50 lines of registers.
" Allow backspacing over everything in insert mode.
set backspace=indent,eol,start "
"
" Path/file expansion in colon-mode.
set wildmode=list:longest "
set wildchar=<TAB> "

" Code folding stuff
set foldmethod=indent
set foldnestmax=10
set nofoldenable

set laststatus=2
set statusline=%t       "tail of the filename
" set statusline+=%{&ff}] "file format
set statusline+=%h      "help file flag
set statusline+=%m      "modified flag
set statusline+=%r      "read only flag
set statusline+=%y      "filetype
set statusline+=%=      "left/right separator
set statusline+=%c,     "cursor column
set statusline+=%l/%L   "cursor line/total lines
set statusline+=\ %P    "percent through file
set statusline+=...[fo=%{&fo}] "format options
set ffs=unix,dos
set ff=unix

"
" Enable syntax-highlighting.
if has("syntax") "
  syntax on "
endif

" Code completion stuff
filetype plugin on
set completeopt=longest,menuone,preview
set omnifunc=syntaxcomplete#Complete
" let g:SuperTabDefaultCompletionType = "<C-X><C-O>"
let g:SuperTabDefaultCompletionType = "context"
let g:SuperTabClosePreviewOnPopupClose = 1
" let g:SuperTabLongestHighlight = 0
" autocmd CursorMovedI * if pumvisible() == 0 && bufname("%") != "[Command Line]"|pclose|endif
" autocmd InsertLeave * if pumvisible() == 0 && bufname("%") != "[Command Line]"|pclose|endif

"------------------------------------------------------------------------------
" Abbreviations.
"------------------------------------------------------------------------------
"
" My name + email address.
ab aga Ashley Anderson III <aganders3@gmail.com>

nnoremap <F5> "=strftime("%Y-%m-%d %H:%M")<CR>P
inoremap <F5> <C-R>=strftime("%Y-%m-%d %H:%M")<CR>

"
"------------------------------------------------------------------------------
" Miscellaneous stuff.
"------------------------------------------------------------------------------
"
" Make p in visual mode replace the selected text with the "" register.
" vnoremap p <Esc>:let current_reg = @"<CR>gvdi<C-R>=current_reg<CR><Esc>"
"
"------------------------------------------------------------------------------
" File-type specific settings.
"------------------------------------------------------------------------------
"
if has("autocmd") "
  " Python code.
  augroup python"
    autocmd BufReadPre,FileReadPre      *.py set tabstop=4"
    autocmd BufReadPre,FileReadPre      *.py set expandtab"
  augroup END"

  au BufRead,BufNewFile *.m4 set filetype=c
endif"
